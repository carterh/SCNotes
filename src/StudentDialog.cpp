#include "Globals.h"
#include <SCModel.h>
#include <Database.h>
#include "view/StudentDialog.h"
#include <iostream>

using namespace SCNotes;

StudentDialog::StudentDialog(BaseObjectType* cobject,
  const Glib::RefPtr<Gtk::Builder>& refBuilder) 
  : Gtk::Dialog(cobject),
    m_builder(refBuilder) {
  
  m_builder->get_widget("StudentCancelButton", m_cancel_button);
  m_builder->get_widget("StudentSaveButton", m_save_button);
  m_builder->get_widget("StudentFirstName", m_first_name);
  m_builder->get_widget("StudentLastName", m_last_name);
  m_builder->get_widget("StudentHomeRoom", m_home_room);
  m_builder->get_widget("StudentNote", m_note);

  m_cancel_button->signal_clicked().connect(sigc::mem_fun(this, 
    &StudentDialog::on_cancel_button_clicked));
  m_save_button->signal_clicked().connect(sigc::mem_fun(this, 
    &StudentDialog::on_save_button_clicked));

  m_refTreeModel = Gtk::ListStore::create(m_columns);
  m_home_room->set_model(m_refTreeModel);

  std::vector<SCModel::HomeRoom> rooms = scmodel.get_home_rooms();
  for (std::vector<SCModel::HomeRoom>::iterator it = rooms.begin(); 
      it != rooms.end(); ++it) {
    Gtk::TreeModel::Row row = *(m_refTreeModel->append());
    row[m_columns.m_col_id] = it->get_id();
    row[m_columns.m_col_name] = it->get_name();
  }

  m_home_room->pack_start(m_columns.m_col_name);

}


StudentDialog::~StudentDialog() {
  ;
}


void StudentDialog::on_cancel_button_clicked() {
  this->close();
}


void StudentDialog::on_save_button_clicked() {
  std::cout << "Save button clicked.\n";
  SCModel::Database db = scmodel.get_db();
  SCModel::Student s(db, 0, m_first_name->get_text(), m_last_name->get_text(),
    m_note->get_buffer()->get_text());
  s.db_add();
  this->close();
}
