// #include <iostream>
#include "Globals.h"
#include "view/MainWindow.h"
#include <iostream>

using namespace SCNotes;

MainWindow::MainWindow(BaseObjectType* cobject, 
  const Glib::RefPtr<Gtk::Builder>& refBuilder) 
: Gtk::Window(cobject),
  m_builder(refBuilder) {

  set_title("SCNotes");

  m_builder->get_widget("NewStudentButton", m_new_student_button);
  m_builder->get_widget("StudentTree", m_student_tree);

  m_new_student_button->signal_clicked().connect(
    sigc::mem_fun(this, &MainWindow::on_new_student_button_clicked));

  this->signal_show().connect(sigc::mem_fun(this, 
    &MainWindow::update_student_table));
}


MainWindow::~MainWindow() {
  ;
}


void MainWindow::on_new_student_button_clicked() {
  m_builder->get_widget_derived("StudentDialog", m_student_dialog);
  m_student_dialog->run();
  m_student_tree->remove_all_columns();
  update_student_table();
}


void MainWindow::update_student_table() {
  std::cout << "Called update student table\n";
  m_refTreeModel = Gtk::ListStore::create(m_student_columns);
  m_student_tree->set_model(m_refTreeModel);

  std::vector<SCModel::Student> students = scmodel.get_students();
  for (std::vector<SCModel::Student>::iterator it = students.begin(); 
      it != students.end(); ++it) {
    Gtk::TreeModel::Row row = *(m_refTreeModel->append());
    row[m_student_columns.m_col_id] = it->get_id();
    row[m_student_columns.m_col_first_name] = it->get_first_name();
    row[m_student_columns.m_col_last_name] = it->get_last_name();
  }

  m_student_tree->append_column("First Name", m_student_columns.m_col_first_name);
  m_student_tree->append_column("Last Name", m_student_columns.m_col_last_name);
}


