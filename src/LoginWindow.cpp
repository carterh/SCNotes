#include <iostream>
#include "Globals.h"
#include "view/LoginWindow.h"
#include "view/MainWindow.h"

using namespace SCNotes;

LoginWindow::LoginWindow(Glib::RefPtr<Gtk::Application> &a) : m_app(a) {
  Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file(
      "glade_project.glade");

  builder->get_widget("LoginWindow", p_win);
  builder->get_widget("LoginButton", p_login_button);
  builder->get_widget("CancelButton", p_cancel_button);
  builder->get_widget("PasswordField", p_pass);
  builder->get_widget("LoginErrorText", p_login_error);

  p_pass->signal_activate().connect(sigc::mem_fun(this, &LoginWindow::on_login_clicked));
  p_login_button->signal_clicked().connect(sigc::mem_fun(this, &LoginWindow::on_login_clicked));
  p_cancel_button->signal_clicked().connect(sigc::mem_fun(this, &LoginWindow::on_cancel_clicked));
}

LoginWindow::~LoginWindow() {
  delete p_login_button;
  delete p_cancel_button;
  delete p_pass;
  delete p_login_error;
  delete p_win;
}

void LoginWindow::on_login_clicked() {
  std::string password = p_pass->get_text();
  std::cout << "Login clicked: " << password <<"\n";
  int val = scmodel.authenticate(password);
  if (val < 0) {
    std::cout << "Authentication failed.\n";
    p_login_error->set_text("Incorrect password.");
  } else if (val == 0) {
    scmodel.init_db();
  } else {
    std::cout << "Successfully authenticated\n";
    m_app->add_window(*main_win);
    main_win->show_all();
    p_win->hide();
  }
    
  std::cout << "Authenticate value: " << val << "\n";
}


void LoginWindow::on_cancel_clicked() {
  std::cout << "Cancel clicked.\n";
  p_win->close();
}

Gtk::Window* LoginWindow::get_window() {
  return p_win;
}
