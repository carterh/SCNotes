#include <gtkmm.h>
#include <iostream>
#include "Globals.h"
#include "view/LoginWindow.h"
#include "view/MainWindow.h"

using namespace SCNotes;

// Note: Declaraitions require explicit namespace.
SCModel::Model SCNotes::scmodel;

MainWindow* SCNotes::main_win = nullptr;

int main(int argc, char *argv[]) {

  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, 
       "org.gtkmm.example");
  std::cout << "Starting...\n";

  LoginWindow login_win(app);
  // This is required to display window contents.
  login_win.get_window()->show_all();

  Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file(
      "glade_project.glade");

  builder->get_widget_derived("MainWindow", main_win);

  //app->add_window(*main_win.get_window());
  // main_win->show_all();
  

  app->run(*login_win.get_window());
  //app->run();

  delete main_win;
  return 0;
}
