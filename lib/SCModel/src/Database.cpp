#define SQLITE_HAS_CODEC
#define SQLITE_TEMP_STORE 2

#include "Database.h"
#include <sqlcipher/sqlite3.h>
#include <iostream>

using namespace SCModel;

Database::Database(std::string file_name) {
  if (sqlite3_open(file_name.c_str(), &m_db)) {
    throw "Failed to open db file.";
  }
}

Database::~Database() {
  sqlite3_close(m_db);
}


int Database::authenticate(std::string password) {

  int output;

  if (sqlite3_key(m_db, password.c_str(), password.length())) {
    throw "Failed to set sqlcypher key.";
  }

  if ((output = get_master_count()) < 0) {
    std::cout << "Get Master Count failed: " << output << std::endl;
  } 

  return output;
}

/// Returns 0 if db empty. Positive number if database is populated. Negative
/// number on read error.
int Database::get_master_count() {
  std::string query = "SELECT count(*) FROM sqlite_master;";
  sqlite3_stmt *stmt;
  int output = -1;
  int val;

  // Size can be -1 for 0 terminated strings.
  if ((val = sqlite3_prepare_v2(m_db, query.c_str(), -1, &stmt, NULL)) == SQLITE_OK) {
    if (sqlite3_step(stmt)) {
      output = sqlite3_column_int(stmt, 0);
    } else {
      std::cout << "Failed to step.\n";
    }
  } else {
    std::cout << "Failed to prepare.\n";
    output = val * -1;
  }

  return output;
}


void Database::execute(const std::string& execute_string) {
  char* err = NULL;
  sqlite3_exec(m_db, execute_string.c_str(), NULL, NULL, &err);
  if (err) {
    std::cout << err << std::endl;
  }
}

std::vector<std::vector<std::string>> Database::query(std::string& query_string) {
  int val;
  sqlite3_stmt *stmt;
  std::vector<std::vector<std::string>> result;

  if (sqlite3_prepare_v2(m_db, query_string.c_str(), -1, &stmt, NULL) == SQLITE_OK) {
    while (sqlite3_step(stmt) == SQLITE_ROW) {
      std::vector<std::string> row;
      std::cout << "Column cout: " << sqlite3_data_count(stmt) << std::endl;
      for (unsigned int i = 0; i < sqlite3_data_count(stmt); i++) {
        std::cout << sqlite3_column_text(stmt, i) << std::endl;
        row.push_back(std::string(reinterpret_cast<const char*>(
          sqlite3_column_text(stmt, i))));
      }
      result.push_back(row);
    }
  }

  sqlite3_finalize(stmt);

  return result;
}

