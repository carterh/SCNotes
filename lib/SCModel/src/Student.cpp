#include "Student.h"
#include <iostream>

using namespace SCModel;

Student::Student(Database &db, int id, std::string first_name, std::string last_name, 
  std::string note) 
  : DBType(db),
    m_id(id),
    m_first_name(first_name),
    m_last_name(last_name),
    m_home_room(0, "Test"),
    m_note(note) {

}


Student::~Student() {
  ;
}


int Student::get_id() {
  return m_id;
}

std::string Student::get_first_name() {
  return m_first_name;
}


std::string Student::get_last_name() {
  return m_last_name;
}


HomeRoom Student::get_home_room() {
  return m_home_room;
}


std::string Student::get_note() {
  return m_note;
}


void Student::db_add() {
  std::string s = "INSERT INTO Students (\
                  first_name, last_name, home_room_id, note) \
                  VALUES (\"" + m_first_name + "\", \"" +
                  m_last_name + "\", " +
                  std::to_string(m_home_room.get_id()) + ", \"" +
                  m_note + "\")";
  std::cout << s << std::endl;
  m_db.execute(s);
}


void Student::db_delete() {
  ;
}


void Student::db_update() {
  ;
}
