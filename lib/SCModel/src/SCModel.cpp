
#include "SCModel.h"
#include <iostream>

using namespace SCModel;

Model::Model() : m_db("scnotes.sqlite3") {
  ;
}

Model::~Model() {
  ;
}

int Model::authenticate(std::string& password) {
  return m_db.authenticate(password);
}

void Model::init_db() {
  std::string home_room = "CREATE TABLE HomeRooms (\
                            id INTEGER PRIMARY KEY,\
                            name TEXT)";

  std::string group = "CREATE TABLE Groups (\
                        id INTEGER PRIMARY KEY,\
                        name TEXT)";

  std::string students = "CREATE TABLE Students (\
                           id INTEGER PRIMARY KEY,\
                           first_name TEXT,\
                           last_name TEXT,\
                           home_room_id INTEGER,\
                           note TEXT,\
                           FOREIGN KEY(home_room_id) REFERENCES HomeRooms(id))";

  std::string meeting = "CREATE TABLE Meetings (\
                          id INTEGER PRIMARY KEY,\
                          title TEXT,\
                          note TEXT,\
                          date INTEGER,\
                          length INTEGER,\
                          group_id INTEGER,\
                          FOREIGN KEY(group_id) REFERENCES Groups(id))";

  std::string attendees = "CREATE TABLE Attendees (\
                            meeting_id INTEGER,\
                            student_id INTEGER,\
                            FOREIGN KEY(meeting_id) REFERENCES Meetings(id),\
                            FOREIGN KEY(student_id) REFERENCES Students(id))";

  m_db.execute(home_room);
  m_db.execute(group);
  m_db.execute(students);
  m_db.execute(meeting);
  m_db.execute(attendees);
}

std::vector<HomeRoom> Model::get_home_rooms() {
  std::string query = "SELECT id, name FROM HomeRooms";
  std::cout << query << std::endl;
  std::vector<std::vector<std::string>> out = m_db.query(query);
  std::vector<HomeRoom> result;
  for (std::vector<std::vector<std::string>>::iterator it = out.begin();
       it != out.end(); ++it) {
    if (it->end() - it->begin() >= 2) {
      std::vector<std::string>::iterator rt = it->begin();
      int id = stoi(*rt, nullptr, 0);
      rt++;
      std::string name = *rt;
      result.push_back(HomeRoom(id, name));
    }
  }
  return result;
}


std::vector<Student> Model::get_students() {
  std::string query = "SELECT id, first_name, last_name, note FROM Students";
  std::vector<std::vector<std::string>> out = m_db.query(query);
  std::vector<Student> result;
  for (std::vector<std::vector<std::string>>::iterator it = out.begin();
       it != out.end(); ++it) {
    if (it->end() - it->begin() >= 3) {
      std::vector<std::string>::iterator rt = it->begin();
      int id = stoi(*rt, nullptr, 0);
      rt++;
      std::string first_name = *rt;
      rt++;
      std::string last_name = *rt;
      rt++;
      std::string note = *rt;
      result.push_back(Student(m_db, id, first_name, last_name, note));
    }
  }
  return result;
}

Database Model::get_db() {
  return m_db;
}
