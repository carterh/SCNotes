
#ifndef HOMEROOM_H_
#define HOMEROOM_H_

namespace SCModel {
  class HomeRoom {
   public:
    HomeRoom(int id, std::string name) : m_id(id), m_name(name) { };

   int get_id() {return m_id;}
   std::string get_name() {return m_name;}

   protected:
    int m_id;
    std::string m_name;
  };
}

#endif
