#ifndef SCDATABASE_H_
#define SCDATABASE_H_

#include <string>
#include <vector>
#include <sqlcipher/sqlite3.h>

namespace SCModel {

  class Database {

   public:
    Database(std::string file);
    ~Database();

    int authenticate(std::string password);
    void execute(const std::string& execute_string);
    std::vector<std::vector<std::string>> query(std::string& query_string);

   protected:
    int get_master_count();

   private:
    sqlite3 *m_db;

  };

}

#endif // SCDATABASE_H_
