#ifndef STUDENT_H_
#define STUDENT_H_

#include <string>
#include "HomeRoom.h"
#include "DBType.h"

namespace SCModel {
  class Student : public DBType {
   public:
    Student(Database &db, int id, std::string first_name, std::string last_name, std::string note);
    ~Student();
    int get_id();
    std::string get_first_name();
    std::string get_last_name();
    HomeRoom get_home_room();
    std::string get_note();
    void db_add();
    void db_delete();
    void db_update();

   protected:
    int m_id;
    std::string m_first_name;
    std::string m_last_name;
    HomeRoom m_home_room;
    std::string m_note;
  };
}

#endif

