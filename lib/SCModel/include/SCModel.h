#ifndef SCMODEL_H_
#define SCMODEL_H_

#include "Database.h"
#include "Student.h"
#include "HomeRoom.h"
#include <vector>

namespace SCModel {

  class Model {
   public:
    Model();
    ~Model();

    int authenticate(std::string& pass);
    void init_db();

    std::vector<HomeRoom> get_home_rooms();
    std::vector<Student> get_students();
    Database get_db();

   protected:
    Database m_db;

  };
}

#endif

