#ifndef DBTYPE_H_
#define DBTYPE_H_

#include "Database.h"

namespace SCModel {
  class DBType {
   public:
    DBType(Database db) : m_db(db) { }
    ~DBType() { };
    virtual void db_add() = 0;
    virtual void db_delete() = 0;
    virtual void db_update() = 0;
   protected:
    Database m_db;
  };
}

#endif // DBTYPE_H_
