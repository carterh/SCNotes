#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <SCModel.h>
#include "view/MainWindow.h"

namespace SCNotes {
  extern SCModel::Model scmodel;
  extern MainWindow* main_win;
}

#endif
