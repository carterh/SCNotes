#ifndef MAIN_WINDOW_H_
#define MAIN_WINDOW_H_

#include <gtkmm.h>
#include "view/StudentDialog.h"

namespace SCNotes {
  class MainWindow : public Gtk::Window {
   public:
    MainWindow(BaseObjectType* cobject, 
      const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~MainWindow();
    void on_new_student_button_clicked();

   protected:
    Glib::RefPtr<Gtk::Builder> m_builder;
    Gtk::Button *m_new_student_button = nullptr;
    Gtk::TreeView *m_student_tree = nullptr;
    StudentDialog *m_student_dialog = nullptr;

    void update_student_table();

    class ModelStudentColumns : public Gtk::TreeModel::ColumnRecord {
     public:
      ModelStudentColumns() { 
        add(m_col_id); 
        add(m_col_first_name); 
        add(m_col_last_name);
      }

      Gtk::TreeModelColumn<int> m_col_id;
      Gtk::TreeModelColumn<Glib::ustring> m_col_first_name;
      Gtk::TreeModelColumn<Glib::ustring> m_col_last_name;
    };

    ModelStudentColumns m_student_columns;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
  };
}

#endif
