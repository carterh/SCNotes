#ifndef STUDENT_DIALOG_H_
#define STUDENT_DIALOG_H_

#include <gtkmm.h>

namespace SCNotes {


  class StudentDialog : public Gtk::Dialog {
   public:
    StudentDialog(BaseObjectType* cobject,
      const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~StudentDialog();

   protected:
    Glib::RefPtr<Gtk::Builder> m_builder;
    Gtk::Button *m_save_button;
    Gtk::Button *m_cancel_button;
    Gtk::Entry *m_first_name;
    Gtk::Entry *m_last_name;
    Gtk::ComboBox *m_home_room;
    Gtk::TextView *m_note;

    void on_cancel_button_clicked();
    void on_save_button_clicked();

    class ModelColumns : public Gtk::TreeModel::ColumnRecord {
     public:
      ModelColumns() { 
        add(m_col_id); 
        add(m_col_name); 
      }

      Gtk::TreeModelColumn<int> m_col_id;
      Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_columns;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
  };
}

#endif
