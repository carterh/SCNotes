#ifndef LOGIN_WINDOW_H_
#define LOGIN_WINDOW_H_

#include <gtkmm.h>

namespace SCNotes {
  class LoginWindow {
   public:
    LoginWindow(Glib::RefPtr<Gtk::Application> &a);
    ~LoginWindow();
    void on_login_clicked();
    void on_cancel_clicked();
    Gtk::Window* get_window();

   private:
    Gtk::Window *p_win = nullptr;
    Gtk::Entry *p_pass = nullptr;
    Gtk::Label *p_login_error = nullptr;
    Gtk::Button *p_login_button = nullptr;
    Gtk::Button *p_cancel_button = nullptr;
    Glib::RefPtr<Gtk::Application> m_app;
  };
}

#endif
