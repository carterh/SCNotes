# Ideas:

## Model Re-Organization

Create a generic *DB Objects* class, which has virtual functions representing various DB function ie: add, delete, update. Then each DB Object (ie: student, meeting) provides that functionality. This will allow us to avoid having a "Model" class with dozens of functions related to various db tables.